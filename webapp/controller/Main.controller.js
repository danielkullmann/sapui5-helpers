sap.ui.define([
  "de/danielkullmann/openui5helpers/controller/BaseController"
], function(Controller) {
  "use strict";

  return Controller.extend("de.danielkullmann.openui5helpers.controller.Main", {});
});
