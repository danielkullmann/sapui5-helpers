sap.ui.define([], function () {
	"use strict";
	return {
		// root package name of the application
		BasePackage: "put your base package here (e.g. com.sap.awesomeapp)",
		AppName: "name of your application"
	};
});