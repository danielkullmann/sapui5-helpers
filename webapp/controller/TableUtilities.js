sap.ui.define([
	"./Settings"
], function(Settings) {
	"use strict";
    return {
    	
        filterTable: function (oController, oConfiguration, oEvent) {
			var sSourceId = oEvent.getSource().getId();
            var sModelName = oConfiguration.ModelName;
            var sFilterFragmentName = oConfiguration.FilterFragmentName || this._throwConfigError("missing FilterFragmentName");
            var aNumericalFields = oConfiguration.NumericalFields || [];
            var oDialog;

			var oModel = oController.getView().getModel(sModelName);

			oController.mSettingsDialogs = oController.mSettingsDialogs || {};
			oDialog = oController.mSettingsDialogs[sFilterFragmentName];

            if (!oDialog) {
				oDialog = sap.ui.xmlfragment({
					fragmentName: Settings.BasePackage + ".fragment." + sFilterFragmentName
				}, oController);
				oDialog.attachEvent("confirm", this._confirmFilterHandler.bind(this, oController, oDialog, oConfiguration, sSourceId));
				oDialog.attachEvent("resetFilters", this._resetFiltersHandler.bind(null, oDialog, aNumericalFields));

				oController.mSettingsDialogs[sFilterFragmentName] = oDialog;
			}

			// toggle compact style
			jQuery.sap.syncStyleClass("sapUiSizeCompact", oController.getView(), oDialog);
			this._updateDialogData(oDialog, oModel, aNumericalFields);
		},
		
		sortTable: function (oController, oConfiguration, oEvent) {
            var sSortFragmentName = oConfiguration.SortFragmentName || this._throwConfigError("missing SortFragmentName");
			var sSourceId = oEvent.getSource().getId();
			var oDialog;

			oController.mSettingsDialogs = oController.mSettingsDialogs || {};
			oDialog = oController.mSettingsDialogs[sSortFragmentName];
			if (!oDialog) {
				oDialog = sap.ui.xmlfragment({
					fragmentName: Settings.BasePackage + ".fragment." + sSortFragmentName
				}, oController);
				oDialog.attachEvent("confirm", this._confirmSortHandler.bind(this, oController, oConfiguration, sSourceId));
				oController.mSettingsDialogs[sSortFragmentName] = oDialog;
			}

			// toggle compact style
			jQuery.sap.syncStyleClass("sapUiSizeCompact", oController.getView(), oDialog);
			oDialog.open();
		},

        _confirmFilterHandler: function (oController, oDialog, oConfiguration, sSourceId, oConfirmEvent) {
            var sFilterButtonId = oConfiguration.FilterButtonId || null;
            var sCollectionId = oConfiguration.CollectionId || this._throwConfigError("missing CollectionId");
            var aNumericalFields = oConfiguration.NumericalFields || [];
			var oCollection = oController.getView().byId(sCollectionId);

            var sFilterString = oConfirmEvent.getParameter("filterString");
            var oBindingData = {};

            /* Filtering */
            oBindingData.filters = [];
            // The list of filters that will be applied to the collection
            var oFilter, bFiltersSet = false;
            var vValueLT, vValueGT;

            for (var i = 0;i < aNumericalFields.length; i++) {
                var sNumericalFieldName = aNumericalFields[i];

                vValueLT = oDialog.getModel().getProperty("/" + sNumericalFieldName + "/vValueLT");
                vValueGT = oDialog.getModel().getProperty("/" + sNumericalFieldName + "/vValueGT");
                if (vValueLT !== "" || vValueGT !== "") {
                    oFilter = this._getCustomFilter(sNumericalFieldName, vValueLT, vValueGT);
                    oBindingData.filters.push(oFilter);
                    sFilterString = sFilterString ? sFilterString + ", " : "Filtered by: ";
                    sFilterString += this._getCustomFilterString(true, sNumericalFieldName, oFilter.sOperator, vValueLT, vValueGT);
                    bFiltersSet = true;
                }
            }

            // Simple filters (String)
            var mSimpleFilters = {};
            var sKey;
            for (sKey in oConfirmEvent.getParameter("filterKeys")) {
                var aSplit = sKey.split("___");
                var sPath = aSplit[1];
                var sValue1 = aSplit[2];
                var oFilterInfo = new sap.ui.model.Filter(sPath, "EQ", sValue1);
                bFiltersSet = true;
                // Creating a map of filters for each path
                if (!mSimpleFilters[sPath]) {
                    mSimpleFilters[sPath] = [oFilterInfo];
                } else {
                    mSimpleFilters[sPath].push(oFilterInfo);
                }
            }

            if (sFilterButtonId) {
                if (bFiltersSet) {
                    oController.getView().byId(sFilterButtonId).setType("Emphasized");
                } else {
                    oController.getView().byId(sFilterButtonId).setType("Transparent");
                }
            }

            for (var path in mSimpleFilters) {
                // All filters on a same path are combined with a OR
                oBindingData.filters.push(new sap.ui.model.Filter(mSimpleFilters[path], false));
            }

            var oBindingInfo = oCollection.getBindingInfo("items");
            var oBindingOptions = this._updateBindingOptions(oController, sCollectionId, oBindingData, sSourceId);
            oCollection.bindAggregation("items", {
                model: oBindingInfo.model,
                path: oBindingInfo.path,
                parameters: oBindingInfo.parameters,
                template: oBindingInfo.template,
                templateShareable: true,
                sorter: oBindingOptions.sorters,
                filters: oBindingOptions.filters
            });

            // Display the filter string if necessary
            if (typeof oCollection.getInfoToolbar === "function") {
                var oToolBar = oCollection.getInfoToolbar();
                if (oToolBar && oToolBar.getContent().length === 1) {
                    oToolBar.setVisible(!!sFilterString);
                    oToolBar.getContent()[0].setText(sFilterString);
                }
            }
        },

		_confirmSortHandler: function (oController, oConfiguration, sSourceId, oConfirmEvent) {
			var sFilterString = oConfirmEvent.getParameter("filterString");
			var sCollectionId = oConfiguration.CollectionId || this._throwConfigError("missing CollectionId");
            var aNumericalFields = oConfiguration.NumericalFields || [];
            var oCollection = oController.getView().byId(sCollectionId);
			var oBindingData = {};

			/* Sorting */
			if (oConfirmEvent.getParameter("sortItem")) {
				var sPath = oConfirmEvent.getParameter("sortItem").getKey();
				oBindingData.sorters = [new sap.ui.model.Sorter(sPath, oConfirmEvent.getParameter("sortDescending"))];
				if (aNumericalFields.indexOf(sPath) >= 0) {
					oBindingData.sorters[0].fnCompare = this._compareNumericValues.bind(this);
				}
			}

			var oBindingInfo = oCollection.getBindingInfo("items");
			var oBindingOptions = this._updateBindingOptions(oController, sCollectionId, oBindingData, sSourceId);
			oCollection.bindAggregation("items", {
				model: oBindingInfo.model,
				path: oBindingInfo.path,
				parameters: oBindingInfo.parameters,
				template: oBindingInfo.template,
				templateShareable: true,
				sorter: oBindingOptions.sorters,
				filters: oBindingOptions.filters
			});

			// Display the filter string if necessary
			if (typeof oCollection.getInfoToolbar === "function") {
				var oToolBar = oCollection.getInfoToolbar();
				if (oToolBar && oToolBar.getContent().length === 1) {
					oToolBar.setVisible(!!sFilterString);
					oToolBar.getContent()[0].setText(sFilterString);
				}
			}
		},
		
		_updateBindingOptions: function (oController, sCollectionId, oBindingData, sSourceId) {
			oController.mBindingOptions = oController.mBindingOptions || {};
			oController.mBindingOptions[sCollectionId] = oController.mBindingOptions[sCollectionId] || {};

			var aSorters = oController.mBindingOptions[sCollectionId].sorters;
			var aGroupby = oController.mBindingOptions[sCollectionId].groupby;

			// If there is no oBindingData parameter, we just need the processed filters and sorters from this function
			if (oBindingData) {
				if (oBindingData.sorters) {
					aSorters = oBindingData.sorters;
				}
				if (oBindingData.groupby || oBindingData.groupby === null) {
					aGroupby = oBindingData.groupby;
				}
				// 1) Update the filters map for the given collection and source
				oController.mBindingOptions[sCollectionId].sorters = aSorters;
				oController.mBindingOptions[sCollectionId].groupby = aGroupby;
				oController.mBindingOptions[sCollectionId].filters = oController.mBindingOptions[sCollectionId].filters || {};
				oController.mBindingOptions[sCollectionId].filters[sSourceId] = oBindingData.filters || [];
			}

			// 2) Reapply all the filters and sorters
			var aFilters = [];
			for (var key in oController.mBindingOptions[sCollectionId].filters) {
				aFilters = aFilters.concat(oController.mBindingOptions[sCollectionId].filters[key]);
			}

			// Add the groupby first in the sorters array
			if (aGroupby) {
				aSorters = aSorters ? aGroupby.concat(aSorters) : aGroupby;
			}

			var aFinalFilters = aFilters.length > 0 ? [new sap.ui.model.Filter(aFilters, true)] : undefined;
			return {
				filters: aFinalFilters,
				sorters: aSorters
			};
		},
		
		applyFiltersAndSorters: function (oController, sControlId) {
			var sAggregationName = "items";
			var oBindingInfo = oController.getView().byId(sControlId).getBindingInfo(sAggregationName);
			var oBindingOptions = this._updateBindingOptions(this, sControlId);
			oController.getView().byId(sControlId).bindAggregation(sAggregationName, {
				model: oBindingInfo.model,
				path: oBindingInfo.path,
				parameters: oBindingInfo.parameters,
				template: oBindingInfo.template,
				templateShareable: true,
				sorter: oBindingOptions.sorters,
				filters: oBindingOptions.filters
			});
		},
		
		_getCustomFilter: function (sPath, vValueLT, vValueGT) {
			var oGtFilter = new sap.ui.model.Filter({
				path: sPath, 
				test: function(value) {
					return parseFloat(value) > parseFloat(vValueGT);
				}
			});
			var oLtFilter = new sap.ui.model.Filter({
				path: sPath, 
				test: function(value) {
					return parseFloat(value) < parseFloat(vValueLT);
				}
			});
			if (vValueLT !== "" && vValueGT !== "") {
				return new sap.ui.model.Filter([oGtFilter, oLtFilter], true);
			}
			if (vValueLT !== "") {
				return oLtFilter;
			}
			return oGtFilter;
		},
		
		_getCustomFilterString: function (bIsNumber, sPath, sOperator, vValueLT, vValueGT) {
			switch (sOperator) {
			case sap.ui.model.FilterOperator.LT:
				return sPath + (bIsNumber ? " (Less than " : " (Before ") + vValueLT + ")";
			case sap.ui.model.FilterOperator.GT:
				return sPath + (bIsNumber ? " (More than " : " (After ") + vValueGT + ")";
			default:
				if (bIsNumber) {
					return sPath + " (More than " + vValueGT + " and less than " + vValueLT + ")";
				}
				return sPath + " (After " + vValueGT + " and before " + vValueLT + ")";
			}
		},
		
		_compareNumericValues: function(value1, value2) {
			if ((value1 === null || value1 === undefined || value1 === "") &&
				(value2 === null || value2 === undefined || value2 === "")) {
				return 0;
			}
			if ((value1 === null || value1 === undefined || value1 === "")) {
				return -1;
			}
			if ((value2 === null || value2 === undefined || value2 === "")) {
				return 1;
			}
			var parsedValue1 = parseFloat(value1);
			var parsedValue2 = parseFloat(value2);
			if(parsedValue1 < parsedValue2) { return -1; }
			if(parsedValue1 === parsedValue2) { return 0; }
			if(parsedValue1 > parsedValue2) { return 1; }
			return 0;
		},

		_resetFiltersHandler: function(oDialog, aNumericalFields) {
			for (var i = 0;i < aNumericalFields.length; i++) {
				var sNumericalFieldName = aNumericalFields[i];
				oDialog.getModel().setProperty("/" + sNumericalFieldName + "/vValueLT", "");
				oDialog.getModel().setProperty("/" + sNumericalFieldName + "/vValueGT", "");
			}
		},

		_updateDialogData: function(oDialog, oModel, aNumericalFields) {
			var oJsonModelDialogData = {};

			var oDialogModel = oDialog.getModel();
			if (!oDialogModel) {
				oDialogModel = new sap.ui.model.json.JSONModel();
				oDialog.setModel(oDialogModel);
			}

			// Loop through each entity
			var aData = oModel.getData();
			aData.forEach(function (oEntity) {
				// Add the distinct properties in a map
				for (var oKey in oEntity) {
					if (!oJsonModelDialogData[oKey]) {
						oJsonModelDialogData[oKey] = [oEntity[oKey]];
					} else if (oJsonModelDialogData[oKey].indexOf(oEntity[oKey]) === -1) {
						oJsonModelDialogData[oKey].push(oEntity[oKey]);
					}
				}
			});
			for (var i = 0;i < aNumericalFields.length; i++) {
				var sNumericalFieldName = aNumericalFields[i];
				var sPrefix = "/"+ sNumericalFieldName;
				oJsonModelDialogData[sNumericalFieldName] = {
					vValueLT: (oDialogModel.getProperty(sPrefix)) ? oDialogModel.getProperty(sPrefix + "/vValueLT") : "",
					vValueGT: (oDialogModel.getProperty(sPrefix)) ? oDialogModel.getProperty(sPrefix + "/vValueGT") : ""
				};
			}

			oDialogModel.setData(oJsonModelDialogData);
			oDialog.open();
		},

        _throwConfigError: function(sMessage) {
            throw new Error("Configuration error:", sMessage);
        }

    };
});