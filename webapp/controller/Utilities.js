sap.ui.define([
	"sap/m/MessageToast",
    "sap/ui/core/format/DateFormat",
	"sap/ui/core/format/NumberFormat",
	"./Settings"
], function (DateFormat) {
	"use strict";

    var oDateFormat = DateFormat.getDateTimeInstance();

	var oNumberFormat = sap.ui.core.format.NumberFormat.getFloatInstance({
        maxFractionDigits: 2,
        groupingEnabled: true
      });

    return {

		formatEpoch: function(sValue) {
			var iValue = parseInt(sValue, 10);
			return oDateFormat.format(new Date(iValue));
		},
		
		parseFloat: function(sValue) {
			return oNumberFormat.parse(sValue);	
		},
		
		showToast: function(sMessage, fnFinish) {
			fnFinish = fnFinish || function() {};
			MessageToast.show(sMessage, {
				onClose: fnFinish,
				duration: 3000,
				at: "center bottom",
				my: "center bottom"
			});
		},
		
		/* Reads data from an OData model via read(), returning
		 * a promise.
		 * Examples:
		 *   Utilities.readTableData(this.getView().getModel(), "/Projekt").then(function(oResponse) {
		 *       var aData = oResponse.results;
		 *       ...
		 *   });
		 *   Utilities.readTableData(this.getView().getModel(), "/Projekt(2)").then(function(oProjekt) {
		 *       ...
		 *   });
		 */
		readTableData: function(oModel, sPath, mParams) {
			mParams = mParams || {};
			return new Promise(function(resolve, reject) {
				var mQueryParams = jQuery.extend({}, mParams, {
					success: resolve,
					error: reject
				});	
				oModel.read(sPath, mQueryParams);
			});
		},
		
		/** Get the value of a column by column id.
		 * 
		 * This is helpful when working with column rows that have no binding attached to it
		 * 
		 * Example:
		 *   _onItemPress: function(oEvent) {
		 *     var sColumnId = "Name"; // this must be defined in the view XML: <Column id="Name" ...>
		 *	   var oCell = Utilities.getTableColumnValue(oEvent, sColumnId);
		 *	   var sValue = oCell.getText(); // or getTitle(), or ...
		 *	 }
		 */
		getTableCellByColumnId: function(oEvent, sColumnId) {
			if (!oEvent) {
				throw new Error("no event given");
			}
			if (!sColumnId) {
				throw new Error("no column id given");
			}
			if (!oEvent.getSource() || !oEvent.getSource().getColumns()) {
				throw new Error("wrong type of source (columns)");
			}
			if (!oEvent.getParameters() || !oEvent.getParameters().listItem || !oEvent.getParameters().listItem.getCells()) { 
				throw new Error("wrong type of source (cells)");
			}
			
			var aColumns = oEvent.getSource().getColumns();
			var aCells = oEvent.getParameters().listItem.getCells();
			for (var i = 0; i < aColumns.length; i++) {
				if (aColumns[i].getId().endsWith("--"+sColumnId)) {
					return aCells[i];
				}
			}
			return null;
		},
		
		
		/** Gets the base URL for destinations.
		 * In case we need to access a destination URL directly (e.g. for the documentservers, 
		 * which are not compliant with OData), we can get the base URL for the destination
		 * with this method. The destination must be defined in /neo-app.json.
		 * Example:
		 *   var sDestinationName = "Java_Server";
		 *	 var sUrl = Utilities.getBaseUrlForDestinations() + sDestinationName + "/files/" + sFolderName;
		 *   oJsonModel.loadData(sUrl, ...);
		 */
		getBaseUrlForDestinations: function() {
			if (!this.destinationsUrl) {
				var userInfo = new sap.ushell.services.UserInfo();
				if (userInfo && userInfo.getUser().getId() === "DEFAULT_USER") {
					// This happens in test scenarios
					this.destinationsUrl = "/destinations/";
				} else {
					this.destinationsUrl = "/sap/fiori/" + Settings.AppName + "/destinations/";
				}
			}
			return this.destinationsUrl;
		},

        /** Gets an item from a list of object, given a key and a value for that key.
         */
		getItemFromList: function(aList, sKey, uValue) {
			for (var i = 0; i < aList.length; i++) {
				var oItem = aList[i];
				if (oItem[sKey] === uValue) {
					return oItem;
				}
			}
			return null;
		}
		
	};
});